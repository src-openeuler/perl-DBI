Name:      perl-DBI
Version:   1.646
Release:   1
Summary:   Database independent interface for Perl
License:   GPL-1.0-or-later OR Artistic-1.0-Perl
URL:       https://dbi.perl.org/
Source0:   https://cpan.metacpan.org/authors/id/T/TI/TIMB/DBI-%{version}.tar.gz
BuildRequires: perl-generators perl-interpreter perl(ExtUtils::MakeMaker) >= 6.76 perl(Test::More)
BuildRequires: perl-devel gcc

%description
The DBI is the standard database interface module for Perl.
It defines a set of methods, variables and conventions that provide
a consistent database interface independent of the actual database being used.
It is important to remember that the DBI is just an interface.
The DBI is a layer of "glue" between an application and one or more database driver modules.
It is the driver modules which do most of the real work. The DBI provides a standard interface
and framework for the drivers to operate within.

%package_help

%prep
%autosetup -n DBI-%{version} -p1
chmod 744 dbixs_rev.pl

for F in dbixs_rev.pl ex/corogofer.pl; do
    perl -MExtUtils::MakeMaker -e "ExtUtils::MM_Unix->fixin(q{$F})"
done
sed -i -e 's/"dbiproxy$ext_pl",//' Makefile.PL
for F in lib/DBD/Gofer/Transport/corostream.pm lib/Bundle/DBI.pm lib/DBD/Proxy.pm lib/DBI/ProxyServer.pm \
    dbiproxy.PL t/80proxy.t lib/DBI/W32ODBC.pm lib/Win32/DBIODBC.pm ;do
    rm "$F"
    sed -i -e '\|^'"$F"'|d' MANIFEST
done

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1 OPTIMIZE="%{optflags}"
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
make test

%files
%license LICENSE
%{_bindir}/*
%{perl_vendorarch}/*

%files help
%doc ex/perl_dbi_nulls_test.pl ex/profile.pl
%doc README.md
%{_mandir}/*/*

%changelog
* Fri Jan 17 2025 Funda Wang <fundawang@yeah.net> - 1.646-1
- update to 1.646

* Sat Apr 09 2022 shixuantong <shixuantong@h-partners.com> - 1.643-3
- fix CVE-2014-10402

* Wed Jun 23 2021 liudabo <liudabo1@huawei.com> - 1.643-2
- Add gcc build dependcy

* Thu Jul 23 2020 xinghe <xinghe1@huawei.com> - 1.643-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 1.643

* Wed May 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.642-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add build requires of perl-devel

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.642-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: change the directory of ex/ and add LICENSE

* Wed Sep 4 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.642-1
- Package init
